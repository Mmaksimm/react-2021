/* eslint-disable react/jsx-no-duplicate-props */
//* eslint-disable no-useless-constructor */
import React, { useState } from 'react'; import PropTypes from 'prop-types';
import { Container, Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const MessageInput = ({
  addNewMessage
}) => {
  const [getText, setText] = useState('')

  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerAddNewMessage = (event) => {
    event.preventDefault();
    addNewMessage(getText);
    setText('');
  }

  return (
    <Container className={`${styles.newMessage} message-input`}>
      <Form onSubmit={handlerAddNewMessage} className={styles.form}>
        <
          input
          className={`${styles.input} message-input-text`}
          placeholder="New Message"
          value={getText}
          onChange={HandlerOnChange}
        />
        <
          Button
          className={`${styles.send} message-input-button`}
          type="submit"
        >
          Send
        </Button>
      </Form>
    </Container>
  )
}

MessageInput.propTypes = {
  addNewMessage: PropTypes.func.isRequired
}

export default MessageInput;
