import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Label, Icon, Comment as MessageUI } from 'semantic-ui-react';
import { getUserImgLink } from '../../helpers/imageHelper';
import getDateForMessage from '../../helpers/getDateForMessage'

import styles from './styles.module.scss';

const Message = ({ message }) => {
  const [isLike, setLike] = useState(false)

  const likeToggleHandler = (event) => {
    event.preventDefault();
    setLike(!isLike);
  }

  const {
    text,
    avatar,
    user,
    createdAt,
  } = message

  const dateForMessage = getDateForMessage(createdAt);

  return (
    <div className={`${styles.separate} message`}>
      <MessageUI.Group className={styles.message}>
        <MessageUI>
          <MessageUI.Avatar
            src={getUserImgLink(avatar)}
            className={`${styles.avatar} message-user-avatar`}
          />
          <MessageUI.Content>
            <MessageUI.Author as="a" className="message-user-name">
              {user}
            </MessageUI.Author>
            <MessageUI.Metadata className="message-time">
              {dateForMessage}
            </MessageUI.Metadata>
            <MessageUI.Text className="message-text" >
              {text}
            </MessageUI.Text>
          </MessageUI.Content>
          <div className={styles.messageAction}>
            <Label
              basic
              size="small"
              as="a"
              className={`${styles.toolbarBtn} ${!isLike ? 'message-like' : 'message-liked'}`}
              onClick={likeToggleHandler}
            >
              <Icon name="thumbs up" className={!isLike ? styles.notLike : null} />
            </Label>
          </div>
        </MessageUI>
      </MessageUI.Group>
    </div>
  );
}

Message.propTypes = {
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [PropTypes.string, PropTypes.bool]
    )
  ).isRequired
};

export default Message;
