import React, { useState } from 'react'
import PropTypes from 'prop-types';
import { Button, Icon, Modal, Form } from 'semantic-ui-react'

import styles from './styles.module.scss';

const EditMessage = ({
  handlerUpdateMessage,
  message,
  handlerCancel
}) => {
  const [getText, setText] = useState(message.text)

  const HandlerOnChange = ({ target: { value } }) => {
    setText(value);
  }

  const handlerUpdate = () => {
    handlerUpdateMessage(message, getText)
  }

  return (
    <Modal open={Boolean(message)} >
      <Modal.Header className={styles.modalHeader}>Edit your message</Modal.Header>
      <Modal.Content className={styles.modalContent}>
        <Modal.Description>
          <Form >
            <Form.TextArea
              name="body"
              value={getText}
              placeholder="What is the news?"
              onChange={HandlerOnChange}
            />
          </Form>
        </Modal.Description>
      </Modal.Content>
      <Modal.Actions className={styles.modalAction}>
        <Button.Group>
          <Button icon onClick={handlerUpdate}>
            <Icon name="save" />
          </Button>
          <Button icon onClick={handlerCancel}>
            <Icon name="window close" />
          </Button>
        </Button.Group>
      </Modal.Actions>
    </Modal>
  )
}

EditMessage.propTypes = {
  handlerUpdateMessage: PropTypes.func.isRequired,
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [PropTypes.string, PropTypes.bool]
    )
  ).isRequired,
  handlerCancel: PropTypes.func.isRequired,
}

export default EditMessage
