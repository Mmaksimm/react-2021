/* eslint-disable no-useless-constructor */
import React from 'react';
import PropTypes from 'prop-types';
import { Button, Icon, Comment as MessageUI } from 'semantic-ui-react';
import getDateForMessage from '../../helpers/getDateForMessage';

import styles from './styles.module.scss';

const OwnMessage = ({
  message,
  modalConfirm,
  editMessage,
}) => {
  const {
    id,
    text,
    createdAt,
  } = message

  const dateForMessage = getDateForMessage(createdAt);

  const handlerEditMessage = () => {
    editMessage(message);
  };

  const handlerModalConfirm = () => {
    modalConfirm(id);
  }


  return (
    <div className={`${styles.separate} own-message`} >
      <MessageUI.Group className={styles.myMessage}>
        <MessageUI>
          <MessageUI.Content>
            <MessageUI.Metadata className="message-time">
              {dateForMessage}
            </MessageUI.Metadata>
            <MessageUI.Text className={`${styles.text} message-text`}>
              {text}
            </MessageUI.Text>
          </MessageUI.Content>
          <div className={styles.messageAction}>
            <Button.Group floated="right" size="mini" className={styles.actionMessage}>
              <Button icon onClick={handlerEditMessage} className="message-edit">
                <Icon name="cog" />
              </Button>
              <Button icon onClick={handlerModalConfirm} className="message-delete">
                <Icon name="trash" />
              </Button>)
            </Button.Group>
          </div>
        </MessageUI>
      </MessageUI.Group>
    </div>
  );
}

OwnMessage.propTypes = {
  message: PropTypes.objectOf(
    PropTypes.oneOfType(
      [PropTypes.string, PropTypes.bool]
    )
  ).isRequired,
  modalConfirm: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired
};

export default OwnMessage;
