/* eslint-disable no-useless-constructor */
import React from 'react';
import PropTypes from 'prop-types';
import { Divider as HorizontalDivider } from 'semantic-ui-react';
import getStartDate from '../../helpers/getStartDate';
import dateForPreview from '../../helpers/dateForPreview';

const Divider = ({ date }) => {
  const today = getStartDate(new Date().toISOString());
  const lastDate = getStartDate(date)
  let dateForView = dateForPreview(date)

  const yesterdayMs = 172800000;
  const todayMs = 86400000;
  if ((today - lastDate) < yesterdayMs) dateForView = 'Yesterday';
  if ((today - lastDate) < todayMs) dateForView = 'Today';

  return (
    <HorizontalDivider horizontal className="messages-divider">
      {dateForView}
    </HorizontalDivider>
  );
}


Divider.propTypes = {
  date: PropTypes.string.isRequired,
};

export default Divider;
