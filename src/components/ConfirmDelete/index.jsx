import React from 'react';
import PropTypes from 'prop-types';
import { Confirm } from 'semantic-ui-react';

const ConfirmDelete = ({
  handlerDeleteMessage,
  messageId,
  handlerCancel
}) => {
  const handlerDelete = event => {
    event.preventDefault();
    handlerDeleteMessage(messageId);
    handlerCancel();
  };

  return (
    <Confirm
      open
      content='Do you want to delete the message?'
      onCancel={handlerCancel}
      onConfirm={handlerDelete}
    />
  );
};

ConfirmDelete.propTypes = {
  handlerDeleteMessage: PropTypes.func.isRequired,
  messageId: PropTypes.string.isRequired,
  handlerCancel: PropTypes.func.isRequired
};

export default ConfirmDelete;
