import React from 'react';
import PropTypes from 'prop-types';
import { Container } from 'semantic-ui-react';
import getDateLastMessage from '../../helpers/getDateLastMessage';

import styles from './styles.module.scss';

const Header = ({ messages }) => {
  let participants = [];
  messages.forEach(message => {
    if (participants.includes(message.userId)) return;
    participants = [...participants, message.userId];
  })

  const numbersMessages = messages.length

  const dateLastMessage = getDateLastMessage(messages);

  return (
    <Container className={`${styles.header} header`}>
      <div className={`${styles.myPaddingLeft} header-title`}>My Chart</div>
      <div className={styles.myPaddingLeft}>
        <span className="header-users-count">{participants.length}</span> participants
      </div>
      <div className={styles.myPaddingLeft}>
        <span className="header-messages-count">{numbersMessages}</span> messages
      </div>
      <div className={styles.myPaddingRight}>
        Last message at <span className="header-last-message-date">{dateLastMessage}</span>
      </div>
    </Container>
  )
};

Header.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [
          PropTypes.string,
          PropTypes.number,
          PropTypes.objectOf(PropTypes.string)
        ]
      )
    )
  )
}

export default Header;
