const getAllMessagesService = () => (
  fetch('https://edikdolynskyi.github.io/react_sources/messages.json')
    .then(res => res.json())
    .then(res => res)
)

export default getAllMessagesService;
