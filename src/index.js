// eslint-disable-next-line no-unused-vars
import React from 'react';
import ReactDOM from 'react-dom';
import Chart from './containers/Chart/index.jsx';

import './styles/index.css'
import 'semantic-ui-css/semantic.min.css';

const root = document.getElementById('root');

ReactDOM.render(<Chart />, root);
