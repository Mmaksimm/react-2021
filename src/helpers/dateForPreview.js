const dateForPreview = date => {
  const week = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

  const datePreview = new Date(Date.parse(date));
  return `${week[datePreview.getDay()]} ${datePreview.getDate()} ${datePreview.getMonth() + 1} ${datePreview.getFullYear()}`
};

export default dateForPreview;