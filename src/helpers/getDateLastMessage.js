import addNullForDate from './addNullForDate';

const getDateLastMessage = messages => {
  const dateLastMessageISO = messages.reduce((prevDate, message) => {
    if (Date.parse(message.createdAt) > Date.parse(prevDate)) return message.createdAt
    return prevDate
  }, new Date(0).toISOString());

  const dateLastMessage = new Date(Date.parse(dateLastMessageISO));

  return `${addNullForDate(dateLastMessage.getDate())
    }.${addNullForDate(dateLastMessage.getMonth() + 1)
    }.${dateLastMessage.getFullYear()
    } ${addNullForDate(dateLastMessage.getHours())
    }:${addNullForDate(dateLastMessage.getMinutes())} `
}

export default getDateLastMessage;
