import addNullForDate from './addNullForDate';

const getDateForMessage = date => {
  const dateMessage = new Date(Date.parse(date));

  return `${addNullForDate(dateMessage.getHours())
    }:${addNullForDate(dateMessage.getMinutes())}`
}

export default getDateForMessage;
