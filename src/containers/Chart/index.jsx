/* eslint-disable no-undef */
import React, { Component } from 'react';
import { Container } from 'semantic-ui-react';
import getAllMessagesService from '../../services/messageService';
import MessageList from '../MessageList'
import Header from '../../components/Header';
import MessageInput from '../../components/MessageInput';
import ConfirmDelete from '../../components/ConfirmDelete';
import EditMessage from '../../components/EditMessage';
import uuid from 'react-uuid';

import styles from './styles.module.scss';

class Chart extends Component {
  constructor(props) {
    super(props);
    this.state = {
      messages: [],
      ownUserId: uuid(),
      user: '',
      editMessage: false,
      confirmDeleteId: '',
    }

    this.handlerAddNewMessage = this.handlerAddNewMessage.bind(this);
    this.handlerUpdateMessage = this.handlerUpdateMessage.bind(this);
    this.handlerDeleteMessage = this.handlerDeleteMessage.bind(this);
    this.handlerEditMessage = this.handlerEditMessage.bind(this);
    this.handlerModalConfirmDeleteMessage = this.handlerModalConfirmDeleteMessage.bind(this);
    this.handlerCancel = this.handlerCancel.bind(this);
  }



  componentDidMount() {
    const getAllMessages = async () => {
      const messages = await getAllMessagesService();
      this.setState({ messages });
    }

    getAllMessages();
  }

  handlerCancel() {
    this.setState({
      editMessage: false,
      confirmDeleteId: ''
    });
  }

  handlerAddNewMessage(text) {
    const newMessage = {
      text,
      id: uuid(),
      userId: this.state.ownUserId,
      avatar: "",
      user: "I",
      createdAt: new Date().toISOString(),
      editedAt: ""
    }
    this.setState({ messages: [...this.state.messages, newMessage] })
  }

  handlerUpdateMessage(oldMessage, text) {
    const updatedMessage = {
      ...oldMessage,
      text,
      editedAt: new Date().toISOString()
    }

    const updateMessages = this.state.messages.map(
      message => message.id !== oldMessage.id ? message : updatedMessage
    );

    this.setState({ messages: updateMessages });
    this.handlerCancel()
  }

  handlerDeleteMessage(id) {
    const updateMessages = this.state.messages.filter(
      message => message.id !== id
    );

    this.setState({ messages: updateMessages })
    this.handlerCancel()
  };

  handlerEditMessage(message) {
    this.setState({ editMessage: message });
  };

  handlerModalConfirmDeleteMessage(id) {
    this.setState({ confirmDeleteId: id });
  };

  render() {
    const messages = this.state.messages;
    return (
      <>
        <Container className={`${styles.chart} chart`}>
          <Header messages={messages} />
          <MessageList
            messages={messages}
            ownUserId={this.state.ownUserId}
            modalConfirm={this.handlerModalConfirmDeleteMessage}
            editMessage={this.handlerEditMessage}
          />
          <MessageInput addNewMessage={this.handlerAddNewMessage} />
        </Container>
        {Boolean(this.state.confirmDeleteId) && (
          <ConfirmDelete
            handlerDeleteMessage={this.handlerDeleteMessage}
            messageId={this.state.confirmDeleteId}
            handlerCancel={this.handlerCancel}
          />
        )}
        {Boolean(this.state.editMessage) && (
          <EditMessage
            handlerUpdateMessage={this.handlerUpdateMessage}
            message={this.state.editMessage}
            handlerCancel={this.handlerCancel}
          />
        )}
      </>
    )
  }
};

export default Chart;
