/* eslint-disable no-undef */
import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Card } from 'semantic-ui-react';
import getStartDate from '../../helpers/getStartDate';
import Preloader from '../../components/Preloader';
import Message from '../../components/Message';
import OwnMessage from '../../components/OwnMessage';
import Divider from '../../components/Divider';

import styles from './styles.module.scss';

const MessageList = ({
  messages,
  ownUserId,
  modalConfirm,
  editMessage
}) => {
  let date = null;
  const startNewDate = ({ createdAt }) => {
    if (!date || (getStartDate(createdAt) - getStartDate(date)) > 86400000) {
      date = new Date(createdAt);
      return true;
    }
    return false
  };

  return (
    <Card className={`${styles.card} message-list`}>
      {
        messages.length
          ? (
            messages
              .sort((messagePrev, message) => (Date.parse(messagePrev.createdAt) - Date.parse(message.createdAt)))
              .map(message => {
                return <Fragment key={message.id}>
                  {startNewDate(message) && <Divider date={message.createdAt} />}
                  {ownUserId !== message.userId
                    ? (
                      <Message
                        message={message}
                      />
                    )
                    : (
                      <OwnMessage
                        key={message.id}
                        message={message}
                        modalConfirm={modalConfirm}
                        editMessage={editMessage}
                      />
                    )
                  }
                </Fragment>
              })
          )
          : <Preloader />
      }
    </Card>
  )
}

MessageList.propTypes = {
  messages: PropTypes.arrayOf(
    PropTypes.objectOf(
      PropTypes.oneOfType(
        [PropTypes.string, PropTypes.bool]
      )
    )
  ),
  ownUserId: PropTypes.string,
  editMessage: PropTypes.func.isRequired,
  modalConfirm: PropTypes.func.isRequired
};

MessageList.defaultProps = {
  messages: [{ id: '' }]
};

export default MessageList;
